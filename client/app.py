"""Simple Async client using aiohttp framework."""

import asyncio
import json
import os

import aiohttp
from flask import Flask, Response
from typeguard import typechecked

SERVER_URL: str = str(os.getenv("SERVER_URL"))
MAX_POOL_SIZE: int = int(os.getenv("MAX_POOL_SIZE", 80))

TOLERATED_STATUS_CODES: tuple = (404,)
SAMPLE_DATA: list = ["azerty", "qsdfgh", "wxcvbn"]

app = Flask(__name__)


@app.route("/404")
@typechecked
def get_not_found_endpoint() -> Response:
    """Mock 404 response."""
    data_list: list = ["not_found"]
    return get_data_endpoint(data_list)


@app.route("/500")
@typechecked
def get_internal_server_error_endpoint() -> Response:
    """Mock 500 response."""
    data_list: list = ["internal_server_error"]
    return get_data_endpoint(data_list)


@app.route("/")
@typechecked
def get_data_endpoint(data_list: list | None = None) -> Response:
    """Get data from synchronous endpoint."""
    http_status: int = 200

    if not data_list:
        data_list = []
    data_list += SAMPLE_DATA

    # Create a new event loop (not setting it globally) and run async functions within it
    loop = asyncio.new_event_loop()
    responses = loop.run_until_complete(make_requests(data_list))

    # Error management
    for response in responses:
        if response.get("error"):
            http_status = int(response["status"])

    response = Response(
        json.dumps(responses), status=http_status, mimetype="application/json"
    )
    return response


@typechecked
async def fetch_data(session, url) -> dict:
    """Fetch data asynchronously (to make multiple GET requests in parallel)."""
    try:
        async with session.get(url) as response:
            if response.status not in TOLERATED_STATUS_CODES:
                response.raise_for_status()
            return await response.json()
    except aiohttp.ClientError as e:
        return {
            "error": str(e),
            "request_info": str(e.request_info),
            "status": str(e.status),
            "message": str(e.message),
            "headers": str(e.headers),
            "history": str(e.history),
            "code": str(e.code),
        }


@typechecked
async def make_requests(data_list) -> list:
    """Make requests using a single session to maintain a single TCP connection"""
    connector = aiohttp.TCPConnector(limit=MAX_POOL_SIZE)
    async with aiohttp.ClientSession(connector=connector) as session:
        tasks = [fetch_data(session, f"{SERVER_URL}/{data}") for data in data_list]
        responses = await asyncio.gather(*tasks)
        return responses


if __name__ == "__main__":
    app.run(debug=True, port=8080, host="0.0.0.0")
