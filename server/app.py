"""Module destined to receive API calls."""

import json
import time

from flask import Flask, Response
from typeguard import typechecked

app = Flask(__name__)


@app.get("/<data>")
@typechecked
def get_info(data: str) -> Response:
    """Get info."""
    http_status: int = 200
    info: dict = {"info": data}

    time.sleep(0.1)
    # trigger different status codes
    if data == "not_found":
        http_status = 404
    if data == "internal_server_error":
        http_status = 500

    response = Response(
        json.dumps(info), status=http_status, mimetype="application/json"
    )
    return response


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8080)
