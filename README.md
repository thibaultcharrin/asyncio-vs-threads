# Understanding Async

This project aims to demonstrate the evolution of API execution strategies, from sequential to Async Event Loops.

## Getting Started

Build and Run the `client` and `server` APIs using docker-compose:

```bash
docker compose up --build   # add -d for a headless execution
```

Test the API interactions using the cURL command:

```bash
curl 127.0.0.1:8080 | jq
```

Try load testing with multiple clients:

```bash
while true; do curl 127.0.0.1:8080 | jq; done
```

Check out error management with the various endpoints:

```bash
curl 127.0.0.1:8080/404 | jq
curl 127.0.0.1:8080/500 | jq
```

## Read On

- [API Execution Strategies](./articles/api-execution-strategies.md)
- [Async Event Loops](./articles/async-event-loops.md)
- [Considerations](./articles/considerations.md)
