# Async Event Loops

## Introduction

To implement a Python Flask API that handles multiple GET requests in parallel using `asyncio` and `aiohttp`, and ensures that these requests are done in a single TCP connection and client session, you can follow the steps outlined below. The key idea is to use Flask's synchronous view function that internally creates an asyncio event loop to manage the asynchronous tasks.

Here’s a step-by-step implementation:

1. **Install Dependencies**: First, ensure that the necessary packages are installed. You can install them via pip:

    ```bash
    pip install flask aiohttp
    ```

2. **Create the Flask API**:

    Here's a Python script that sets up a Flask API with the required functionality:

    ```python
    from flask import Flask, jsonify
    import asyncio
    import aiohttp

    app = Flask(__name__)

    # Asynchronous function to make multiple GET requests in parallel
    async def fetch_data(session, url):
        async with session.get(url) as response:
            return await response.json()

    async def make_requests(urls):
        # Using a single session to maintain a single TCP connection
        async with aiohttp.ClientSession() as session:
            tasks = [fetch_data(session, url) for url in urls]
            responses = await asyncio.gather(*tasks)
            return responses

    # Synchronous endpoint in Flask
    @app.route('/fetch-data')
    def fetch_data_endpoint():
        urls = [
            'https://api.example.com/data1',
            'https://api.example.com/data2',
            'https://api.example.com/data3'
        ]

        # Create a new event loop and run async functions within it
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        responses = loop.run_until_complete(make_requests(urls))

        return jsonify(responses)

    if __name__ == '__main__':
        app.run(debug=True)
    ```

## Explanation:

### 1. Asynchronous Function (fetch_data):

- This function is responsible for making individual GET requests using the aiohttp session. It fetches the data asynchronously.

### 2. Parallel Requests (make_requests):

- Here, an asynchronous context is established using aiohttp.ClientSession, which ensures that all requests are made using a single TCP connection. Multiple GET requests are made in parallel using asyncio.gather.

### 3. Synchronous Endpoint (fetch_data_endpoint):

- This is a Flask endpoint that handles HTTP requests synchronously. Inside this function, a new asyncio event loop is created using asyncio.new_event_loop().

- The event loop is then used to run the asynchronous function make_requests, which makes the actual GET requests in parallel.

### 4. Running the Application:

- The Flask app runs in debug mode by default for testing purposes. When a request hits the /fetch-data endpoint, the server will make multiple GET requests to the specified URLs in parallel and return the aggregated results as a JSON response.

## How It Works:

- When a client sends a request to the /fetch-data endpoint, the Flask API executes the synchronous function fetch_data_endpoint.

- This function creates an asyncio event loop and uses it to run the asynchronous function make_requests.

- The make_requests function uses aiohttp to create a single client session, and within that session, multiple GET requests are made in parallel.

- The results of these requests are gathered and returned as a JSON response to the client.

## Notes:

- Client Session: The use of a single aiohttp.ClientSession() ensures that all requests share a single TCP connection, which is critical for efficient resource usage and performance.

- Event Loop Management: Flask operates synchronously, so managing the event loop manually with asyncio.new_event_loop() and loop.run_until_complete() allows for the integration of asynchronous tasks within a synchronous API context.

This setup should fulfill your requirement of making multiple asynchronous GET requests in parallel while ensuring the use of a single TCP connection.

## Read On

- [Considerations](./considerations.md)