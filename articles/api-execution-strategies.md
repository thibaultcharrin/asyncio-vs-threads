# Evolution of API Execution Strategies: From Sequential to Async Event Loops

## Introduction

As the usage of our REST APIs grew, the demand for efficient execution of tasks within the APIs became critical. Initially, all functions were executed sequentially, which was simple but inefficient for I/O-bound tasks like making external API calls. To improve performance, we transitioned to using threads for parallel execution. However, threading began to strain Kubernetes resources under heavy loads. Finally, we adopted asynchronous programming with `asyncio` and `aiohttp`, which provided a scalable solution without overwhelming system resources.

In this article, we'll walk through the evolution of our API execution strategy, providing code examples for each approach.

## 1. Sequential Execution

Sequential execution is the simplest form of execution, where tasks are executed one after another. This is inefficient for I/O-bound tasks, as the CPU waits for each task to complete before moving on to the next one.

### Example:

```python
import requests

def fetch_data(url):
    response = requests.get(url)
    return response.json()

def process_data():
    urls = [
        'https://api.example.com/data1',
        'https://api.example.com/data2',
        'https://api.example.com/data3'
    ]
    
    results = []
    for url in urls:
        data = fetch_data(url)
        results.append(data)
    
    return results

if __name__ == "__main__":
    data = process_data()
    print(data)
```

**Drawback**: In the sequential approach, each request is made one after the other, causing delays when making multiple external API calls.

## 2. Parallel Execution Using Threads
To overcome the delay caused by sequential execution, we moved to a threaded approach. By leveraging Python's threading module, we were able to execute multiple I/O-bound tasks in parallel. This approach works well initially but can lead to resource contention as the number of threads increases, especially in a Kubernetes environment.

### Example:

```python
import requests
from concurrent.futures import ThreadPoolExecutor

def fetch_data(url):
    response = requests.get(url)
    return response.json()

def process_data():
    urls = [
        'https://api.example.com/data1',
        'https://api.example.com/data2',
        'https://api.example.com/data3'
    ]
    
    with ThreadPoolExecutor(max_workers=5) as executor:
        results = list(executor.map(fetch_data, urls))
    
    return results

if __name__ == "__main__":
    data = process_data()
    print(data)
```

**Improvement**: This approach reduces the total time taken to fetch data from multiple URLs by fetching them in parallel using threads.

**Drawback**: Threading can become resource-intensive. As the number of threads increases, it can lead to thread contention, higher memory consumption, and eventually resource exhaustion in Kubernetes environments.

## 3. Async Execution Using asyncio and aiohttp

To address the limitations of threading, we transitioned to asynchronous programming using Python’s `asyncio` and `aiohttp` libraries. This approach allows for efficient handling of I/O-bound tasks by using non-blocking code, which results in better scalability without consuming excessive resources.

## Example:

```python
import aiohttp
import asyncio

async def fetch_data(session, url):
    async with session.get(url) as response:
        return await response.json()

async def process_data():
    urls = [
        'https://api.example.com/data1',
        'https://api.example.com/data2',
        'https://api.example.com/data3'
    ]
    
    async with aiohttp.ClientSession() as session:
        tasks = [fetch_data(session, url) for url in urls]
        results = await asyncio.gather(*tasks)
    
    return results

if __name__ == "__main__":
    data = asyncio.run(process_data())
    print(data)
```

**Improvement**: By using asyncio with aiohttp, the program can handle multiple requests concurrently without creating new threads. This is achieved by using an event loop that waits for tasks to complete, allowing the program to handle other tasks in the meantime.

**Advantages**:

- **Resource Efficiency**: Reduces the need for multiple threads, which consumes fewer system resources.

- **Scalability**: Handles a large number of I/O-bound tasks concurrently without blocking the main thread.

- **Performance**: Significantly reduces the time required to execute multiple I/O-bound tasks.

## Conclusion

Through the evolution from sequential execution to threaded execution and finally to async execution using `asyncio` and `aiohttp`, we were able to significantly improve the performance and scalability of our REST APIs. The asynchronous approach provided the most efficient use of resources while delivering high performance, especially in a Kubernetes environment where resource optimization is critical.

This transition not only improved our system's performance but also prepared our infrastructure for future growth and scalability demands. Newcomers to our team can build upon this foundation and continue optimizing our API infrastructure as needed.

## Read On

- [Async Event Loops](./async-event-loops.md)

